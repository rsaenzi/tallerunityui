﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatingCube : MonoBehaviour {

	// Aqui listamos todas las pantallas que se pueden cargar, desde esta pantalla
	public GameObject prefabWin;


	void OnCollisionEnter(Collision collision) {

		// Este gameObject es que el contiene a todas las pantallas, es decir, el canvas
		Transform screenCanvas = GameObject.Find ("Canvas").transform;

		// Crea una copia del prefab, es decir, recrea un gameObject con la informacion contenida en el prefab
		Instantiate (prefabWin, screenCanvas, false);

		// Elimina de la Jerarquia (Hierarchy) la pantalla Game
		GameObject screenGame = GameObject.Find ("Canvas/ScreenGame(Clone)");
		Destroy (screenGame);
	}
}
