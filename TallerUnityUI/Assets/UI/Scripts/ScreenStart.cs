﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenStart : MonoBehaviour {
	
	// Aqui listamos todas las pantallas que se pueden cargar, desde esta pantalla
	public GameObject prefabCredits;
	public GameObject prefabOptions;
	public GameObject prefabGame;

	public GameObject prefabLevel1;


	public void goToScreenCredits() {

		// Este gameObject es que el contiene a todas las pantallas, es decir, el canvas
		Transform screenCanvas = GameObject.Find ("Canvas").transform;

		// Crea una copia del prefab, es decir, recrea un gameObject con la informacion contenida en el prefab
		Instantiate (prefabCredits, screenCanvas, false);

		// Elimina de la Jerarquia (Hierarchy) este gameObject, es decir, esta pantalla
		Destroy (this.gameObject);
	}


	public void goToScreenOptions() {
		
		// Este gameObject es que el contiene a todas las pantallas, es decir, el canvas
		Transform screenCanvas = GameObject.Find ("Canvas").transform;

		// Crea una copia del prefab, es decir, recrea un gameObject con la informacion contenida en el prefab
		Instantiate (prefabOptions, screenCanvas, false);

		// Elimina de la Jerarquia (Hierarchy) este gameObject, es decir, esta pantalla
		Destroy (this.gameObject);
	}


	public void goToScreenGame() {

		// Este gameObject es que el contiene a todas las pantallas, es decir, el canvas
		Transform screenCanvas = GameObject.Find ("Canvas").transform;

		// Crea una copia del prefab, es decir, recrea un gameObject con la informacion contenida en el prefab
		Instantiate (prefabGame, screenCanvas, false);

		// Elimina de la Jerarquia (Hierarchy) este gameObject, es decir, esta pantalla
		Destroy (this.gameObject);

		// ----------

		// Este gameObject es que el contiene a todas las pantallas, es decir, el canvas
		Transform containerLevels = GameObject.Find ("Levels").transform;

		// Crea una copia del prefab, es decir, recrea un gameObject con la informacion contenida en el prefab
		Instantiate (prefabLevel1, containerLevels, false);
	}
}
