﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenWin : MonoBehaviour {

	// Aqui listamos todas las pantallas que se pueden cargar, desde esta pantalla
	public GameObject prefabStart;


	public void goToScreenStart() {

		// Este gameObject es que el contiene a todas las pantallas, es decir, el canvas
		Transform screenCanvas = GameObject.Find ("Canvas").transform;

		// Crea una copia del prefab, es decir, recrea un gameObject con la informacion contenida en el prefab
		Instantiate (prefabStart, screenCanvas, false);

		// Elimina de la Jerarquia (Hierarchy) este gameObject, es decir, esta pantalla
		Destroy (this.gameObject);

		// --------------

		// Buscamos el nivel a eliminar
		GameObject level1 = GameObject.Find ("Levels/Level1(Clone)");

		// Lo eliminamos
		Destroy (level1);
	}
}
